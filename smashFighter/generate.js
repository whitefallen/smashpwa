require('dotenv').config({path: '../.env'});
const fs = require('fs');
const mongoose = require('mongoose');
const fighterDatas = require('./smashAssets/fighters/fighter.json');
const Fighter = require('./mDbSchema/fighter');
// this is our MongoDB database
const dbRoute =
    `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}/smash`;

// connects our back end code with the database
mongoose.connect(dbRoute, { useNewUrlParser: true });

let db = mongoose.connection;

db.once('open', () => console.log('connected to the database'));

// checks if connection with the database is successful
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

let fighterSavedCounter = 0;
for (const fighter of fighterDatas.fighters) {
    let fighterSchema = new Fighter();
    let fighterData = {
        fighter: {
            fighterId: fighter.displayNum,
            nameDe: fighter.displayName.de_DE.replace("<br>", ""),
            nameEn: fighter.displayName.en_US.replace("<br>", ""),
            nameFile: fighter.file,
            renderImg: `../smashFighter/smashAssets/assets/render/${fighter.file}/${fighter.file}_main.png`,
            stockImg: `../smashFighter/smashAssets/assets/stock/${fighter.file}/${fighter.file}_stock.png`
        }
    };
    fighterSchema.fighter = fighterData.fighter;
    fighterSchema.save(function (err) {
        if (err) {
            console.log(err);
        }
        fighterSavedCounter++;
        if (fighterSavedCounter === fighterDatas.fighters.length) {
            console.log("All Saved");
            db.close();
        }
    });
}

