var fs = require('fs');
var request = require('request');

let rawdata = fs.readFileSync('./smashAssets/fighters/fighter.json');
let fighters = JSON.parse(rawdata);

var download = function(uri, filename, callback){
    request.head(uri, function(err, res, body){
        console.log('content-type:', res.headers['content-type']);
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};

var createDir = function() {
    fighters.fighters.forEach((fighter) => {
        let dirRender = `./smashAssets/assets/render/${fighter.file}`;
        let dirStock = `./smashAssets/assets/stock/${fighter.file}`;
        if (!fs.existsSync(dirRender)){
            fs.mkdirSync(dirRender);
        }
        if (!fs.existsSync(dirStock)){
            fs.mkdirSync(dirStock);
        }
    });
};


var downloadRender = function () {
    fighters.fighters.forEach((fighter) => {
        let dirRender = `./smashAssets/assets/render/${fighter.file}`;
        console.log(fighter.file);

        download(`https://www.smashbros.com/assets_v2/img/fighter/${fighter.file}/main.png`, `${dirRender}/${fighter.file}_main.png`, function(){
            console.log('done');
        });
        /*
        for(let i=1; i < 8; i++) {
            download(`https://www.smashbros.com/assets_v2/img/fighter/${fighter.file}/main${i}.png`, `${dirRender}/${fighter.file}_main${i}.png`, function(){
                console.log('done');
            });
        }*/
    });
};

var downloadStock = function() {
    fighters.fighters.forEach((fighter) => {
        let dirStock = `./smashAssets/assets/stock/${fighter.file}`;
        download(`https://www.smashbros.com/assets_v2/img/fighter/pict/${fighter.file}.png`, `${dirStock}/${fighter.file}_stock.png`, function(){
            console.log('done');
        });
    });
};

createDir();
downloadRender();
downloadStock();

