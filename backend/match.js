// /backend/data.js
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// this will be our data base's data structure
const MatchSchema = new Schema(
    {
        data : {
            players: {
                playerOne : {
                    playerOneName: String,
                    playerOneChar: String,
                    playerOneStock: String
                },
                playerTwo : {
                    playerTwoName: String,
                    playerTwoChar: String,
                    playerTwoStock: String
                }
            },
            winner: String
        }
    },
    { timestamps: true }
);

// export the new Schema so we could modify it using Node.js
module.exports = mongoose.model("Match", MatchSchema);
