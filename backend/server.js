require('dotenv').config({path: '../.env'});

const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
const Fighter = require('./fighter');
const Match = require('./match');
const path = require('path');
const Sentry = require('@sentry/node');

const API_PORT = 7010;
const app = express();

Sentry.init({ dsn: 'https://94bd7bd23ef74165ac6a48c59d952aab@sentry.io/1540991' });

app.use(cors());

const router = express.Router();
router.use(Sentry.Handlers.requestHandler());

// this is our MongoDB database
const dbRoute =
    `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}/smash`;

// connects our back end code with the database
mongoose.connect(dbRoute, { useNewUrlParser: true });

let db = mongoose.connection;

db.once('open', () => console.log('connected to the database'));

// checks if connection with the database is successful
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// (optional) only made for logging and
// bodyParser, parses the request body to be a readable json format
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger('dev'));
app.use(express.static(path.join(__dirname, '../client/build')));
app.use(express.static(path.join(__dirname, '..')));


router.get('/getSmashFighters', (req, res) => {
    Fighter.find((err, data) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true, data: data });
    });
});

router.post('/addMatch', (req, res) => {
    let matchSchema = new Match();

    const data = req.body;

    if (!data) {
        return res.json({
            success: false,
            error: 'INVALID INPUTS',
        });
    }

    matchSchema.data = data.data;
    matchSchema.save((err) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true });
    });
});

router.get('/debug-sentry', function mainHandler(req, res) {
    throw new Error('My first Sentry error!');
});

// The error handler must be before any other error middleware and after all controllers
router.use(Sentry.Handlers.errorHandler());
// Optional fallthrough error handler
router.use(function onError(err, req, res, next) {
    // The error id is attached to `res.sentry` to be returned
    // and optionally displayed to the user for support.
    res.statusCode = 500;
    res.end(res.sentry + "\n");
});

// append /api for our http requests
app.use('/api', router);

// Handles any requests that don't match the ones above
app.get('*', (req,res) =>{
    res.sendFile(path.join(__dirname+'/../client/build/index.html'));
});

// launch our backend into a port
app.listen(process.env.PORT || API_PORT, () => console.log(`LISTENING ON PORT ${process.env.PORT || API_PORT}`));
