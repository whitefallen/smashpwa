// /backend/data.js
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// this will be our data base's data structure
const FighterSchema = new Schema(
    {
        fighter : {
            fighterId: String,
            nameDe: String,
            nameEn: String,
            nameFile: String,
            renderImg: String,
            stockImg: String
        }
    },
    { timestamps: true }
);

// export the new Schema so we could modify it using Node.js
module.exports = mongoose.model("Fighter", FighterSchema);
