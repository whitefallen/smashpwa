// /client/App.js
import React, { Component } from 'react';
import axios from 'axios';
import {Button, Container, Header, Label, Segment, Form} from 'semantic-ui-react';

class App extends Component {
    // initialize our state
    constructor(props) {
        super(props);
        this.state = {
            players:[],
            smashFighters:[]
        }

    }
    componentDidMount() {
        this.getSmashFightersFromDb();
    }

    componentWillUnmount() {
    }

    getSmashFightersFromDb = () => {
        fetch(`${process.env.REACT_APP_HEROKU_URL}/api/getSmashFighters`)
            .then((data) => data.json())
            .then((res) => this.setState({ smashData: res.data }))
            .then(() => {
                let smashFighters = this.state.smashData.map((smashData) => smashData.fighter);
                smashFighters.forEach((fighter) => {
                    this.setState({
                        smashFighters: this.state.smashFighters.concat({
                            text: fighter.nameEn,
                            value: fighter.nameEn,
                            key: fighter.nameEn,
                            image: {
                                avatar: true, src: fighter.stockImg
                            }
                        })
                    })
                })
            })
    };

    resetState = () => {
        console.log("pressed");
        this.setState({
            playerOneName: '',
            playerOneCharacter: '',
            playerOneStock: '',
            playerTwoName: '',
            playerTwoCharacter: '',
            playerTwoStock: ''
        });
    };

    insertMatchData = (playerOneName, playerOneChar, playerOneStock, playerTwoName, playerTwoChar, playerTwoStock) => {
        if(
            playerOneName &&
            playerOneChar &&
            playerOneStock &&
            playerTwoName &&
            playerTwoChar &&
            playerTwoStock
        ) {
            let winner;
            if(playerOneStock > playerTwoStock) {
                winner = playerOneName;
            } else if(playerTwoStock > playerOneStock) {
                winner = playerTwoName;
            } else {
                winner = "-";
            }
            let matchData = {
                data: {
                    players: {
                        playerOne: {
                            playerOneName,
                            playerOneChar,
                            playerOneStock,
                        },
                        playerTwo : {
                            playerTwoName,
                            playerTwoChar,
                            playerTwoStock,
                        },
                    },
                    winner : winner
                }
            };
            axios.post(`${process.env.REACT_APP_HEROKU_URL}/api/addMatch`, matchData);
        } else {

        }
    };

    render() {
        const smashFighters = this.state.smashFighters;
        return (
            <div>
                <Container style={{marginTop: 3 + 'em'}}>
                    <div style={{textAlign: "left"}}>
                        <img src={"/ssbu.png"} style={{maxWidth: 40+"%", height: "auto"}} alt={"Super smash bros ultimate Logo"}/>
                    </div>
                    <Header as='h1' dividing>Welcome to Smash Bros Ultimate PWA </Header>
                    <p>
                        This is a little App, to gain experince in PWA development, and in usage of the MERN Stack.
                    </p>
                    <Segment.Group>
                        <Segment>
                            <Form.Field>
                                <Label>Player One Name</Label>
                                <Form.Input
                                    placeholder='Name'
                                    onChange={(e) => this.setState({playerOneName: e.target.value})}
                                    value={this.state.playerOneName}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Label>Player One Character</Label>
                                <Form.Dropdown
                                    placeholder='Character'
                                    search
                                    selection
                                    options={smashFighters}
                                    onChange={(e) => this.setState({playerOneCharacter: e.target.textContent})}
                                    value={this.state.playerOneCharacter}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Label>Player One Stock</Label>
                                <Form.Input
                                    placeholder='Stock'
                                    onChange={(e) => this.setState({playerOneStock: e.target.value})}
                                    value={this.state.playerOneStock}
                                />
                            </Form.Field>
                        </Segment>
                        <Segment>
                            <Form.Field>
                                <Label>Player Two Name</Label>
                                <Form.Input
                                    placeholder='Name'
                                    onChange={(e) => this.setState({playerTwoName: e.target.value})}
                                    value={this.state.playerTwoName}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Label>Player Two Character</Label>
                                <Form.Dropdown
                                    placeholder='Character'
                                    search
                                    selection
                                    options={smashFighters}
                                    onChange={(e) => this.setState({playerTwoCharacter: e.target.textContent})}
                                    value={this.state.playerTwoCharacter}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Label>Player Two Stock</Label>
                                <Form.Input
                                    placeholder='Stock'
                                    onChange={(e) => this.setState({playerTwoStock: e.target.value})}
                                    value={this.state.playerTwoStock}
                                />
                            </Form.Field>
                        </Segment>
                        <Segment>
                            <Button primary onClick={() => {
                                this.insertMatchData(this.state.playerOneName, this.state.playerOneCharacter, this.state.playerOneStock, this.state.playerTwoName, this.state.playerTwoCharacter, this.state.playerTwoStock);
                                this.resetState();
                            }}>
                                Submit Match
                            </Button>
                        </Segment>
                    </Segment.Group>
                </Container>
            </div>
        );
    }
}

export default App;
